import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'product_list.dart';
import 'product_grid.dart';
import 'stack_widgets.dart';
import 'employees_table.dart';
import 'row_of_offers.dart';
import 'table_of_offers.dart';
import 'wrapped_offers.dart';

void main() {
  runApp(const ListExample());
}

class ListExample extends StatelessWidget {
  const ListExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('Row of Offers'),
          ),
          body: const SafeArea(
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Center(
                  child: WrappedOffers(),
                //TableOfOffers(), //RowOfOffers()
              ),
            ),
          ),
          // appBar: AppBar(
          //   title: const Text('Table'),
          // ),
          // body: const SafeArea(
          //   child: Padding(
          //     padding: EdgeInsets.all(8.0),
          //     child: Center(child: EmployeesTable()),
          //   ),
          // ),
          // appBar: AppBar(
          //   title: const Text('Stack'),
          // ),
          // body: const StackWidgets(),
          // appBar: AppBar(
          //   title: const Text('GridView'),
          // ),
          // body: const ProductGrid(),
          // appBar: AppBar(
          //   title: const Text('ListView'),
          // ),
          // body: const ProductList(),
        ),
      ),
    );
  }
}